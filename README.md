[![License](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://github.com/gorhill/uBlock/blob/master/LICENSE.txt)

Fork of popular ad blocker [uBlock](https://github.com/gorhill/uBlock)